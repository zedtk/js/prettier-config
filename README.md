# @zedtk/prettier-config

[![GitLab pipeline](https://gitlab.com/zedtk/js/prettier-config/badges/main/pipeline.svg)](https://gitlab.com/zedtk/js/prettier-config/-/commits/main)

[![npm](https://img.shields.io/npm/v/@zedtk/prettier-config?logo=npm)](https://www.npmjs.com/package/@zedtk/prettier-config)
[![Snyk Known Vulnerabilities](https://img.shields.io/snyk/vulnerabilities/npm/@zedtk/prettier-config?logo=snyk)](https://snyk.io/test/npm/@zedtk/prettier-config)

![License](https://img.shields.io/badge/license-MIT-brightgreen)

@zedtk/prettier-config is an opinionated configuration for prettier.

## Prerequisites

Before you begin, ensure you have met the following requirements:

-   You have installed `prettier` version 2 or higher

## Installing @zedtk/prettier-config

To install @zedtk/prettier-config, follow these steps:

npm:

```shell
npm i -D @zedtk/prettier-config
```

pnpm:

```shell
pnpm add -D @zedtk/prettier-config
```

## Using @zedtk/prettier-config

To use @zedtk/prettier-config, follow these steps:

.prettierrc.json

```json
"@zedtk/prettier-config"
```

You can extend the configuration as explained in [prettier documentation](https://prettier.io/docs/en/configuration.html#sharing-configurations):

.prettierrc.js

```js
module.exports = {
    ...require("@zedtk/prettier-config"),
    semi: false,
};
```

## License

This project uses the following license: [MIT](https://gitlab.com/zedtk/js/prettier-config/-/blob/main/LICENCE.txt)
