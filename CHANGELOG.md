## [1.0.1](https://gitlab.com/zedtk/js/prettier-config/compare/v1.0.0...v1.0.1) (2021-08-22)


### Bug Fixes

* package content ([df9bec3](https://gitlab.com/zedtk/js/prettier-config/commit/df9bec346b2d2db94ac8ffb5f02dc516fedb4563))

# 1.0.0 (2021-08-22)


### Features

* prettier configuration ([f02d679](https://gitlab.com/zedtk/js/prettier-config/commit/f02d6790599f2ce770894b2f284e988e276aa92e))
